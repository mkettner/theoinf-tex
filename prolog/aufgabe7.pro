% (1) - (8)
direktverbindung(esens, wilhelmshaven).
direktverbindung(wilhelmshaven, oldenburg).
direktverbindung(oldenburg, osnabrueck).
direktverbindung(wilhelmshaven, osnabrueck).
direktverbindung(bad_zwischenahn, oldenburg).
direktverbindung(oldenburg, bremen).
direktverbindung(bad_zwischenahn, bremen).
direktverbindung(bremen, verden).

% (9)
direktverbindung(X,Y) :-
    direktverbindung(Y,X).
% (10a)
verbindung(X,Y) :-
    direktverbindung(X,Z),
    direktverbindung(Z,Y).

% (10b)
verbindung(X,Y) :-
    direktverbindung(X,Z),
    direktverbindung(Z,W),
    direktverbindung(W,Y).

% (10c)
verbindung(X, Y) :-
    direktverbindung(X,Y).
