#!/usr/bin/env python
from sys import argv
from math import log

rn = int(argv[1]) # Ebenen
n = 2**rn         # maximaler Node-indice
nb = 2**(rn+1)    # maximaler Blaetter-indice

print "digraph C {"
print "rankdir=TB;"

for i in xrange(1,nb):
    if i < n: # normale Nodes
        print "x%d[label=\"x%d\"];" % (i,int(log(i)/log(2))+1)
        print "x%d->x%d[style=dotted,label=\"0\"];" % (i, 2*i)
        print "x%d->x%d[label=\"1\"];" % (i, 2*i+1)
    else: # Blaetter
        print "x%d[shape=box,label=\"lbl_x%d\"];" % (i,i)

print "}"
